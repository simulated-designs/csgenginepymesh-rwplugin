#ifndef CSGENGINEPyMesh_HPP_
#define CSGENGINEPyMesh_HPP_

#include <rw/core/Ptr.hpp>
#include <rw/core/Plugin.hpp>
#include <rw/geometry/CSGEngine.hpp>

#include <Boolean/BooleanEngine.h>
#include <string>

class CSGEnginePyMesh : public rw::geometry::CSGEngine
{
public:
  using Ptr = rw::core::Ptr<CSGEngine>;

  CSGEnginePyMesh(std::string engine_type = "igl") : engine_type(engine_type) {}

  /**
   * @brief get the String id of the engine
   * @return std::string
   */
  std::string getID() const { return "PyMesh-" + engine_type; }

  /**
   * @brief Create a Union of two TriMeshes
   * @param m1 First TriMesh
   * @param m2 Second TriMesh
   * @return The Resulting TriMesh created as a new Shared pointer
   */
  rw::geometry::TriMeshData::Ptr Union(rw::geometry::TriMeshData::Ptr m1, rw::geometry::TriMeshData::Ptr m2) const;

  /**
   * @brief Create a Union of two TriMeshes
   * @param m1 First TriMesh
   * @param m2 Second TriMesh
   * @return The Resulting TriMesh created as a new Shared pointer
   */
  rw::geometry::TriMeshData::Ptr Difference(rw::geometry::TriMeshData::Ptr m1, rw::geometry::TriMeshData::Ptr m2) const;

  /**
   * @brief Create a Union of two TriMeshes
   * @param m1 First TriMesh
   * @param m2 Second TriMesh
   * @return The Resulting TriMesh created as a new Shared pointer
   */
  rw::geometry::TriMeshData::Ptr Intersection(rw::geometry::TriMeshData::Ptr m1, rw::geometry::TriMeshData::Ptr m2) const;

  /**
   * @brief Create a Union of two TriMeshes
   * @param m1 First TriMesh
   * @param m2 Second TriMesh
   * @return The Resulting TriMesh created as a new Shared pointer
   */
  rw::geometry::TriMeshData::Ptr SymmetricDifference(rw::geometry::TriMeshData::Ptr m1, rw::geometry::TriMeshData::Ptr m2) const;

private:
  rw::core::Ptr<PyMesh::BooleanEngine> prepareEngine(rw::geometry::TriMeshData::Ptr, rw::geometry::TriMeshData::Ptr) const;
  rw::geometry::TriMeshData::Ptr getEngineRes(rw::core::Ptr<PyMesh::BooleanEngine> engine) const;

  std::string engine_type;
};

/**
 * @brief A plugin providing CSGEnginePyMesh for RobWork.
 */
class CSGEnginePyMeshPlugin : public rw::core::Plugin
{
public:
  //! @brief Construct new plugin
  CSGEnginePyMeshPlugin();

  //! @brief Destructor
  virtual ~CSGEnginePyMeshPlugin();

  //! @copydoc rw::core::Plugin::getExtensionDescriptors
  std::vector<rw::core::Extension::Descriptor> getExtensionDescriptors();

  //! @copydoc rw::core::Plugin::makeExtension
  rw::core::Ptr<rw::core::Extension> makeExtension(const std::string &id);

  //! @brief Register the plugins extensions in the rw::core::ExtensionRegistry.
  static void registerPlugin();
};

#endif
