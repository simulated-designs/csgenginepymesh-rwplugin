
set(PyMesh_FOUND TRUE)
# ##################################################################################################
# Find Include Dir
# ##################################################################################################
find_file(
    PyMesh_main_INCLUDE_DIR
    NAMES MeshFactory.h
    HINTS "${PyMesh_ROOT}" "$ENV{PyMesh_ROOT}" "${PyMesh_INCLUDE_DIR}"
          "${CMAKE_CURRENT_LIST_DIR}/PyMesh"
    PATH_SUFFIXES src
)
find_file(
    PyMesh_tools_INCLUDE_DIR
    NAMES Boolean/BooleanEngine.h
    HINTS "${PyMesh_ROOT}" "$ENV{PyMesh_ROOT}" "${PyMesh_INCLUDE_DIR}"
          "${CMAKE_CURRENT_LIST_DIR}/PyMesh"
    PATH_SUFFIXES tools
)

if(EXISTS ${PyMesh_main_INCLUDE_DIR} AND EXISTS ${PyMesh_tools_INCLUDE_DIR})
    get_filename_component(PyMesh_main_INCLUDE_DIR ${PyMesh_main_INCLUDE_DIR} PATH)
    get_filename_component(PyMesh_tools_INCLUDE_DIR ${PyMesh_tools_INCLUDE_DIR} PATH)
    get_filename_component(PyMesh_tools_INCLUDE_DIR ${PyMesh_tools_INCLUDE_DIR} PATH)

    set(PyMesh_INCLUDE_DIRS ${PyMesh_main_INCLUDE_DIR} ${PyMesh_tools_INCLUDE_DIR})

    set(PyMesh_LIBRARIES)
    foreach(lib tbb Mesh MeshUtils Triangle Boolean)

        # ##########################################################################################
        # Find Library
        # ##########################################################################################
        find_library(
            PyMesh_${lib}
            NAMES "libPyMesh-${lib}.so" "lib${lib}.so"
            HINTS "${PyMesh_ROOT}" "$ENV{PyMesh_ROOT}" "${CMAKE_CURRENT_LIST_DIR}/PyMesh"
            PATH_SUFFIXES python/pymesh/lib python/pymesh/third_party/lib
        )
        #message(STATUS "found ${PyMesh_${lib}}")
        if(EXISTS ${PyMesh_${lib}})
            #message(STATUS "  - add Library: PyMesh::${lib}")
            add_library(PyMesh::${lib} UNKNOWN IMPORTED)
            set_target_properties(
                PyMesh::${lib} PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${PyMesh_INCLUDE_DIRS}"
                                          IMPORTED_LOCATION ${PyMesh_${lib}}
            )
            set(PyMesh_LIBRARIES ${PyMesh_LIBRARIES} PyMesh::${lib})
        else()
            set(PyMesh_FOUND FALSE)
            #message(ERROR "Could not find library: ${lib}")
            return()
        endif()

    endforeach()

else()
    set(PyMesh_FOUND FALSE)
    #message(ERROR "Could not find include folder")
    return()
endif()

set_target_properties(PyMesh::Boolean PROPERTIES INTERFACE_LINK_LIBRARIES "PyMesh::Triangle")
set_target_properties(PyMesh::Triangle PROPERTIES INTERFACE_LINK_LIBRARIES "PyMesh::MeshUtils")
set_target_properties(PyMesh::MeshUtils PROPERTIES INTERFACE_LINK_LIBRARIES "PyMesh::Mesh")
set_target_properties(PyMesh::Mesh PROPERTIES INTERFACE_LINK_LIBRARIES "PyMesh::tbb")

# ##################################################################################################
# Finalize find package
# ##################################################################################################

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PyMesh DEFAULT_MSG PyMesh_LIBRARIES PyMesh_INCLUDE_DIRS)

mark_as_advanced(PyMesh_LIBRARIES PyMesh_INCLUDE_DIRS)

