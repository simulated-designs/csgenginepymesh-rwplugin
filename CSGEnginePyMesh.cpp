#include <iostream>
#include "CSGEnginePyMesh.hpp"

using namespace rw::geometry;
using namespace rw::core;

namespace
{
    bool isEmpty(rw::geometry::TriMeshData::Ptr mesh)
    {
        return mesh->_triangles.rows() == 0 || mesh->_vertecies.rows() == 0;
    }
}

rw::core::Ptr<PyMesh::BooleanEngine> CSGEnginePyMesh::prepareEngine(rw::geometry::TriMeshData::Ptr lhs, rw::geometry::TriMeshData::Ptr rhs) const
{
    rw::core::Ptr<PyMesh::BooleanEngine> engine = PyMesh::BooleanEngine::create(engine_type);
    engine->set_mesh_1(lhs->_vertecies, lhs->_triangles.cast<int>());
    engine->set_mesh_2(rhs->_vertecies, rhs->_triangles.cast<int>());
    return engine;
}

rw::geometry::TriMeshData::Ptr CSGEnginePyMesh::getEngineRes(rw::core::Ptr<PyMesh::BooleanEngine> engine) const
{
    rw::core::Ptr<TriMeshData> res = rw::core::ownedPtr(new TriMeshData());
    engine->clean_up();
    res->_triangles = engine->get_faces().cast<uint32_t>();
    res->_vertecies = engine->get_vertices();

    return res;
}

TriMeshData::Ptr CSGEnginePyMesh::Union(TriMeshData::Ptr m1, TriMeshData::Ptr m2) const
{
    if (m1.isNull() || m2.isNull())
    {
        RW_THROW("Got Null Data");
    }
    if (isEmpty(m1))
    {
        return ownedPtr(new TriMeshData(*m2));
    }
    if (isEmpty(m2))
    {
        return ownedPtr(new TriMeshData(*m1));
    }
    rw::core::Ptr<PyMesh::BooleanEngine> engine = prepareEngine(m1, m2);
    engine->compute_union();
    return getEngineRes(engine);
}

TriMeshData::Ptr CSGEnginePyMesh::Difference(TriMeshData::Ptr m1, TriMeshData::Ptr m2) const
{
    if (m1.isNull() || m2.isNull())
    {
        RW_THROW("Got Null Data");
    }
    if (isEmpty(m1))
    {
        return ownedPtr(new TriMeshData(*m2));
    }
    if (isEmpty(m2))
    {
        return ownedPtr(new TriMeshData(*m1));
    }
    rw::core::Ptr<PyMesh::BooleanEngine> engine = prepareEngine(m1, m2);
    engine->compute_difference();
    return getEngineRes(engine);
}

TriMeshData::Ptr CSGEnginePyMesh::Intersection(TriMeshData::Ptr m1, TriMeshData::Ptr m2) const
{
    if (m1.isNull() || m2.isNull())
    {
        RW_THROW("Got Null Data");
    }
    if (isEmpty(m1))
    {
        return ownedPtr(new TriMeshData(*m2));
    }
    if (isEmpty(m2))
    {
        return ownedPtr(new TriMeshData(*m1));
    }
    rw::core::Ptr<PyMesh::BooleanEngine> engine = prepareEngine(m1, m2);
    engine->compute_intersection();
    return getEngineRes(engine);
}

TriMeshData::Ptr CSGEnginePyMesh::SymmetricDifference(TriMeshData::Ptr m1, TriMeshData::Ptr m2) const
{
    if (m1.isNull() || m2.isNull())
    {
        RW_THROW("Got Null Data");
    }
    if (isEmpty(m1))
    {
        return ownedPtr(new TriMeshData(*m2));
    }
    if (isEmpty(m2))
    {
        return ownedPtr(new TriMeshData(*m1));
    }
    rw::core::Ptr<PyMesh::BooleanEngine> engine = prepareEngine(m1, m2);
    engine->compute_symmetric_difference();
    return getEngineRes(engine);
}

RW_ADD_PLUGIN(CSGEnginePyMeshPlugin)

CSGEnginePyMeshPlugin::CSGEnginePyMeshPlugin() : Plugin("CSGEnginePyMeshPlugin", "CSGEnginePyMeshPlugin", "1.0")
{
}

CSGEnginePyMeshPlugin::~CSGEnginePyMeshPlugin()
{
}

std::vector<rw::core::Extension::Descriptor> CSGEnginePyMeshPlugin::getExtensionDescriptors()
{
    std::vector<Extension::Descriptor> exts;
    exts.push_back(Extension::Descriptor("CSGEnginePyMesh-igl", "rw.geometry.CSGEngine"));
    exts.back().getProperties().set<std::string>("EngineID", CSGEnginePyMesh().getID());
    exts.push_back(Extension::Descriptor("CSGEnginePyMesh-cork", "rw.geometry.CSGEngine"));
    exts.back().getProperties().set<std::string>("EngineID", CSGEnginePyMesh("cork").getID());

    return exts;
}

//! @copydoc rw::core::Plugin::makeExtension
rw::core::Ptr<rw::core::Extension> CSGEnginePyMeshPlugin::makeExtension(const std::string &id)
{
    std::string arg;
    if (id == "CSGEnginePyMesh-igl")
    {
        arg = "igl";
    }
    else if (id == "CSGEnginePyMesh-cork")
    {
        arg = "cork";
    }

    static const CSGEnginePyMesh::Ptr engine = ownedPtr(new CSGEnginePyMesh(arg));

    const Extension::Ptr extension = ownedPtr(
        new Extension("CSGEnginePyMesh-" + arg, "rw.geometry.CSGEngine", this, engine.cast<CSGEngine>()));

    extension->getProperties().set<std::string>("EngineID", engine->getID());
    return extension;
}

//! @brief Register the plugins extensions in the rw::core::ExtensionRegistry.
void CSGEnginePyMeshPlugin::registerPlugin()
{
    ExtensionRegistry::getInstance()->registerExtensions(ownedPtr(new CSGEnginePyMeshPlugin()));
}
